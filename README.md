# Yandex Widget

This is a Ruby wrapper for Yandex Kassa Widget API. This is very simple solution to initialize payments, make auto payments and check payment status. 
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'yandex_widget'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yandex_widget

## Usage

Configure the gem with the credentials provided by Yandex Kassa in an initializer.

```ruby
YandexWidget.configure do |config|
  config.store_id = 'store_id'
  config.secret_key = 'secret_key'
end
```

#### Create payment
To initialize widget you need to generate `confirmation_token` from Yandex Kassa

```ruby
YandexWidget::Client.init(100, 'Idempotence Key', params)
```
It will return [payment object](https://checkout.yandex.com/developers/payment-forms/widget#widget-initialization), use it to initialize widget.

More about available options you can read in the [official documentation](https://checkout.yandex.com/developers/payment-forms/widget#widget-initialization)

#### Check payment status
```ruby
YandexWidget::Client.payment_info('Payment Id')
```
It will return [payment object](https://checkout.yandex.com/developers/api#payment_object)

#### Cancel payment
It's only available for payments with status `waiting_for_capture`
```ruby
YandexWidget::Client.cancel('Payment Id', 'Idempotence Key')
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/dcrt/yandex_widget. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Yandex Widget project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/dcrt/yandex_widget/blob/master/CODE_OF_CONDUCT.md).
