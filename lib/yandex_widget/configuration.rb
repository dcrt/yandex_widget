module YandexWidget
  class Configuration
    attr_accessor :store_id, :secret_key

    def initialize
      @store_id = nil
      @secret_key = nil
    end
  end
end
