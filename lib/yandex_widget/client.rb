module YandexWidget
  class Client

    def self.init(amount, idempotence_key, params = {})
      params = default_init_params(amount).merge!(params)
      YandexWidget::Requests.new('', idempotence_key, params: params).init_client
    end

    def self.payment_info(payment_id)
      YandexWidget::Requests.new(payment_id).request_info
    end

    def self.capture(payment_id, idempotence_key, amount)
      YandexWidget::Requests.new("#{payment_id}/capture", idempotence_key, params: amount_params(amount)).set_status
    end

    def self.cancel(payment_id, idempotence_key)
      YandexWidget::Requests.new("#{payment_id}/cancel", idempotence_key).set_status
    end

    def self.default_init_params(amount)
      amount_params(amount).merge(
        confirmation: {
          type: 'embedded'
        },
        capture: true,
        description: ''
      )
    end

    def self.amount_params(amount)
      {
        amount: {
          value: amount,
          currency: 'RUB'
        }
      }
    end
  end
end
