module YandexWidget
  class Requests
    BASE_URL = 'https://payment.yandex.net/api/v3/payments/'

    def initialize(path, idempotence_key = nil, params: {})
      @url = BASE_URL + path
      @idempotence_key = idempotence_key
      @params = params
    end

    def init_client
      HTTParty.post(@url, body: @params.to_json, headers: default_headers,
                          basic_auth: auth, format: :json).parsed_response
    end

    def request_info
      HTTParty.get(@url, basic_auth: auth, format: :json).parsed_response
    end

    def set_status
      HTTParty.post(@url, body: @params.to_json, headers: default_headers,
                          basic_auth: auth, format: :json)
    end

    private

    def default_headers
      {
        'Idempotence-Key': @idempotence_key,
        'Content-Type': 'application/json'
      }
    end

    def auth
      {
        username: YandexWidget.configuration.store_id,
        password: YandexWidget.configuration.secret_key
      }
    end
  end
end
