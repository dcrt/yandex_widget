require 'httparty'
require 'yandex_widget/configuration'
require 'yandex_widget/requests'
require 'yandex_widget/client'
require 'yandex_widget/version'

module YandexWidget
  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end
end
