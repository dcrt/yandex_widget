lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'yandex_widget/version'

Gem::Specification.new do |spec|
  spec.name          = 'yandex_widget'
  spec.version       = YandexWidget::VERSION
  spec.authors       = ['Mikhail Matyukhin']
  spec.email         = ['itsmatyukhin@gmail.com']

  spec.summary       = 'Integration for Yandex Kassa Widget'
  spec.description   = 'Ruby Yandex Kassa Widget integration'
  spec.homepage      = 'https://gitlab.com/dcrt/yandex_widget'
  spec.license       = 'MIT'

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_runtime_dependency 'httparty', '~> 0.14'
end
